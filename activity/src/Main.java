// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("Nicko Andrei","+639123456789", "Paombong, Bulacan");
        Contact contact2 = new Contact("John Doe", "+639122334455","Quezon City");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("The phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println("Name: " + contact.getName());
                System.out.println("Contact Name: " + contact.getContactName());
                System.out.println("Address: " + contact.getAddress());
                System.out.println("-----------------------------");
            }
        }
    }


}