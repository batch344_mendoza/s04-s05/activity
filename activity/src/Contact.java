public class Contact {
    private String name;
    private String contactName;

    private String address;

    public Contact(){}

    public Contact(String name, String contactName, String address){
        this.name = name;
        this.contactName = contactName;
        this.address = address;

    }

    public String getName(){
        return this.name;
    }
    public String getContactName(){
        return this.contactName;
    }
    public String getAddress(){
        return this.address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
